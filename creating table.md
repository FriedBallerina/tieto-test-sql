CREATE TABLE soogikohad 
(nimi TEXT,
asukoht TEXT,
keskmineHinne INTEGER,
hinnanguteArv INTEGER);

INSERT INTO soogikohad (nimi, asukoht, keskmineHinne, hinnanguteArv) VALUES ('Lusikas', 'Tammsaare tee 11',5, 10);
INSERT INTO soogikohad (nimi, asukoht, keskmineHinne, hinnanguteArv) VALUES ('Kahvel', 'Wiedemanni 23',7, 10);
INSERT INTO soogikohad (nimi, asukoht, keskmineHinne, hinnanguteArv) VALUES ('Nuga', 'Punane 55', 6, 10);

SELECT * FROM soogikohad;